Author: Martina Ferrari <tina@debian.org>
Date:   Mon Jan 3 01:41:22 2022 +0000
Forwarded: https://github.com/google/mtail/issues/609
Description: Skip logfiles that cannot be read
  The Tailer.PollLogPatterns and Tailer.Ignore methods currently abort they
  encounter files that cause errors on os.Stat or os.Getwd, which can be caused
  by race conditions (file removed after filepath.Glob was called) or broken
  symlinks. This can make mtail crash on start-up, but also prevents good log
  files from being processed due to the early return of PollLogPatterns.
  .
  This patch changes PollLogPatterns to log the errors and continue the
  operation. In Ignore the errors now are handled by returning `true`, so the
  file is ignored.

Index: mtail/internal/tailer/tail.go
===================================================================
--- mtail.orig/internal/tailer/tail.go
+++ mtail/internal/tailer/tail.go
@@ -227,20 +227,22 @@ func (t *Tailer) AddPattern(pattern stri
 	return nil
 }
 
-func (t *Tailer) Ignore(pathname string) (bool, error) {
+func (t *Tailer) Ignore(pathname string) bool {
 	absPath, err := filepath.Abs(pathname)
 	if err != nil {
-		return false, err
+		glog.V(2).Infof("Couldn't get absolute path for %q: %s", pathname, err)
+		return true
 	}
 	fi, err := os.Stat(absPath)
 	if err != nil {
-		return false, err
+		glog.V(2).Infof("Couldn't stat path %q: %s", pathname, err)
+		return true
 	}
 	if fi.Mode().IsDir() {
 		glog.V(2).Infof("ignore path %q because it is a folder", pathname)
-		return true, nil
+		return true
 	}
-	return t.ignoreRegexPattern != nil && t.ignoreRegexPattern.MatchString(fi.Name()), nil
+	return t.ignoreRegexPattern != nil && t.ignoreRegexPattern.MatchString(fi.Name())
 }
 
 func (t *Tailer) SetIgnorePattern(pattern string) error {
@@ -362,16 +364,14 @@ func (t *Tailer) PollLogPatterns() error
 		}
 		glog.V(1).Infof("glob matches: %v", matches)
 		for _, pathname := range matches {
-			ignore, err := t.Ignore(pathname)
-			if err != nil {
-				return err
-			}
+			ignore := t.Ignore(pathname)
 			if ignore {
 				continue
 			}
 			absPath, err := filepath.Abs(pathname)
 			if err != nil {
-				return err
+				glog.V(2).Infof("Couldn't get absolute path for %q: %s", pathname, err)
+				continue
 			}
 			glog.V(2).Infof("watched path is %q", absPath)
 			if err := t.TailPath(absPath); err != nil {
Index: mtail/internal/tailer/tail_test.go
===================================================================
--- mtail.orig/internal/tailer/tail_test.go
+++ mtail/internal/tailer/tail_test.go
@@ -183,6 +183,35 @@ func TestTailerOpenRetries(t *testing.T)
 	testutil.ExpectNoDiff(t, expected, received, testutil.IgnoreFields(logline.LogLine{}, "Context"))
 }
 
+func TestTailerUnreadableFile(t *testing.T) {
+	// Test broken files are skipped.
+	ta, lines, awaken, dir, stop := makeTestTail(t)
+
+	brokenfile := filepath.Join(dir, "brokenlog")
+	logfile := filepath.Join(dir, "log")
+	testutil.FatalIfErr(t, ta.AddPattern(brokenfile))
+	testutil.FatalIfErr(t, ta.AddPattern(logfile))
+
+	glog.Info("create logs")
+	testutil.FatalIfErr(t, os.Symlink("/nonexistent", brokenfile))
+	f := testutil.TestOpenFile(t, logfile)
+
+	testutil.FatalIfErr(t, ta.PollLogPatterns())
+	testutil.FatalIfErr(t, ta.PollLogStreams())
+
+	glog.Info("write string")
+	testutil.WriteString(t, f, "\n")
+	awaken(1)
+
+	stop()
+
+	received := testutil.LinesReceived(lines)
+	expected := []*logline.LogLine{
+		{context.Background(), logfile, ""},
+	}
+	testutil.ExpectNoDiff(t, expected, received, testutil.IgnoreFields(logline.LogLine{}, "Context"))
+}
+
 func TestTailerInitErrors(t *testing.T) {
 	var wg sync.WaitGroup
 	_, err := New(context.TODO(), &wg, nil)
